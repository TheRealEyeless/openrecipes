#!/bin/bash

echo "FIXME: Paths not correct anymore"
exit 1

cd `dirname "${BASH_SOURCE[0]}"`

W=(48 72 96 144 192)
DPINAME=("mdpi" "hdpi" "xhdpi" "xxhdpi" "xxxhdpi")

for ((ii=0;ii<5;ii++));
do
	echo "Processing icon.svg for ${DPINAME[$ii]}@${W[$ii]} px"
	suffix=${DPINAME[$ii]}
	resdir="android-sources/res"
	dirname=$resdir/drawable-${DPINAME[$ii]}
	fname="$dirname"/icon.png
	inkscape -z -f=img/icon.svg --export-png="$fname" \
		-h=${W[$ii]}
done
