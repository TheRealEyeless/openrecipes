/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import org.jschwab.recipes 1.0

Page {
	ModalDialog {
		id: importErrorDialog

		standardButtons: Dialog.Ok

		title: qsTr("Error")
		ColumnLayout {
			anchors.fill: parent

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("The import of the recipe failed.")
				wrapMode: Text.Wrap
			}
		}
	}

	OpenRecipeDialog {
		id: openRecipeDialog
		onAccepted: if (!backend.importRecipeFromFile(file)) importErrorDialog.open()
	}

	ModalDialog {
		id: newRecipeDialog

		standardButtons: Dialog.Cancel

		title: qsTr("New recipe")

		ColumnLayout {
			anchors.fill: parent

			Button {
				text: qsTr("Create new recipe")
				onClicked: function () {
					stackView.editRecipe();
					newRecipeDialog.accept();
				}
				highlighted: Globals.mobile
				Layout.fillWidth: true
			}

			Button {
				text: qsTr("Import recipe from file")
				onClicked: function () {
					openRecipeDialog.open();
					newRecipeDialog.accept();
				}
				Layout.fillWidth: true
			}
		}
	}

	ModalDialog {
		id: syncFailedDialog
		standardButtons: Dialog.Ok

		title: qsTr("Syncing failed")

		ColumnLayout {
			anchors.fill: parent

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("The synchronization failed.")
				wrapMode: Text.Wrap
			}
		}
	}

	ModalDialog {
		id: stopSyncDialog
		standardButtons: Dialog.NoButton

		title: qsTr("Stopping sync…")

		ColumnLayout {
			anchors.fill: parent

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("Please wait whyle stoping sync.")
				wrapMode: Text.Wrap
			}
		}

		Connections {
			target: backend
			onSynchronizingDone: stopSyncDialog.accept();
		}
	}

	ModalDialog {
		id: syncDialog
		standardButtons: Dialog.Cancel

		title: qsTr("Syncing data…")

		ColumnLayout {
			anchors.fill: parent

			ProgressBar {
				Layout.alignment: Qt.AlignHCenter
				indeterminate: true
			}

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("Please wait while synchronization is in progress.")
				wrapMode: Text.Wrap
			}
		}

		onRejected: {
			backend.stopSynchronizing();
			stopSyncDialog.open();
		}

		Connections {
			target: backend
			onSynchronizingDone: {
				syncDialog.accept();
				if (!success) syncFailedDialog.open();
			}
		}
	}

	Rectangle {
		anchors.fill: parent
		color: Globals.accentColor
		opacity: 0.05
		visible: !Globals.mobile
	}

	header: ToolBar {
		RowLayout {
			anchors.fill: parent

			Image {
				source: "qrc:/img/icon_plain.svg"
				sourceSize.height: parent.height - 10
				Layout.leftMargin: 5
				visible: Globals.mobile
			}

			BigLabel {
				text: qsTr("Recipes")
				horizontalAlignment: Qt.AlignHCenter
				verticalAlignment: Qt.AlignVCenter
				Layout.fillWidth: true
			}

			ToolButton {
				icon.name: "view-refresh"
				icon.source: "qrc:/img/fallback-icons/view-refresh.svg"
				onClicked: function() {
					backend.synchronize();
					syncDialog.open();
				}
				ToolTip.text: qsTr("Synchronize with server")
				ToolTip.delay: 500
				ToolTip.visible: hovered
				visible: backend.syncAvailable
			}

			ToolButton {
				icon.name: "configure"
				icon.source: "qrc:/img/fallback-icons/configure.svg"
				onClicked: stackView.toSettings()
				ToolTip.text: qsTr("Settings")
				ToolTip.delay: 500
				ToolTip.visible: hovered
			}
		}
	}

	MediumLabel {
		anchors.centerIn: parent
		text: qsTr("Create your first recipe.")
		wrapMode: Text.Wrap
		visible: backend.recipesList.length == 0 && filter.text.length == 0
	}

	MediumLabel {
		anchors.centerIn: parent
		text: qsTr("No recipe found.")
		wrapMode: Text.Wrap
		visible: backend.recipesList.length == 0 && filter.text.length != 0
	}

	ColumnLayout {
		anchors.fill: parent

		RowLayout {
			spacing: 0

			TextField {
				id: filter
				Layout.fillWidth: true
				Layout.leftMargin: Globals.mobile ? 5 : 0
				placeholderText: qsTr("Search recipe…")
				text: backend.filter
				onTextChanged: backend.filter = text
			}

			ToolButton {
				icon.name: "edit-clear"
				icon.source: "qrc:/img/fallback-icons/edit-clear.svg"
				onClicked: filter.text = ""
				ToolTip.text: qsTr("Clear filter")
				ToolTip.delay: 500
				ToolTip.visible: hovered
			}
		}

		ListView {
			id: recipesList
			Layout.fillWidth: true
			Layout.fillHeight: true

			orientation: ListView.Vertical
			model: backend.recipesList
			ScrollBar.vertical: ScrollBar {}
			boundsBehavior: Flickable.StopAtBounds
			clip: true
			delegate: Item {
				width: parent.width
				height: Math.max(name.height, image.height) + 10
				RoundedImage {
					id: image
					anchors.verticalCenter: parent.verticalCenter
					anchors.left: parent.left
					anchors.leftMargin: 5
					height: Globals.recipesListImageHeight
					width: height
					fillMode: Image.PreserveAspectCrop
					source: modelData.smallImage
				}
				Label {
					id: name
					anchors.leftMargin: 5
					anchors.rightMargin: 5
					anchors.left: image.right
					anchors.right: parent.right
					anchors.verticalCenter: parent.verticalCenter
					text: modelData.name
					wrapMode: Text.Wrap
					Component.onCompleted: {
						font.pixelSize = font.pixelSize * 1.2;
					}
				}
				Rectangle {
					height: parent.width
					width: 1
					rotation: 90
					anchors.verticalCenter: parent.bottom
					anchors.horizontalCenter: parent.horizontalCenter
					gradient: Gradient {
						GradientStop{
							position: 0.0
							color: "transparent"
						}
						GradientStop{
							position: 0.5
							color: (index == recipesList.count - 1) ? "transparent" : Globals.accentColor
						}
						GradientStop{
							position: 1.0
							color: "transparent"
						}
					}
				}

				MouseArea {
					anchors.fill: parent
					onClicked: function() {
						recipesList.currentIndex = index;
						recipesList.select(modelData);
					}
				}
			}
			highlight: Rectangle {
				color: Globals.mobile ? "transparent" : Globals.accentColor
			}
			highlightMoveDuration: 0
			highlightResizeDuration: 0
			Component.onCompleted: function() {
				if (!Globals.mobile && count != 0) mainRowLayout.selectRecipe(model[currentIndex]);
			}
			function select(recipe) {
				if (Globals.mobile) {
					stackView.selectRecipe(recipe);
				} else {
					mainRowLayout.selectRecipe(recipe);
				}
			}
			onModelChanged: function() {
				if(!Globals.mobile && count != 0) mainRowLayout.selectRecipe(model[currentIndex])
			}
		}
	}

	RoundButton {
		anchors.bottom: parent.bottom
		anchors.right: parent.right
		anchors.bottomMargin: 5
		anchors.rightMargin: 5

		icon.name: "list-add"
		icon.source: "qrc:/img/fallback-icons/list-add.svg"
		onClicked: {
			if (Globals.mobile) {
				stackView.editRecipe();
			} else {
				newRecipeDialog.open();
			}
		}
		ToolTip.text: qsTr("Add new recipe")
		ToolTip.delay: 500
		ToolTip.visible: hovered
		highlighted: Globals.mobile
		Component.onCompleted: {
			if (Globals.mobile) {
				icon.width = icon.width * 1.8;
				icon.height = icon.height * 1.8;
			}
		}
	}

	function highlightRecipe(id) {
		recipesList.currentIndex = backend.getRecipeListIndex(id);
	}
}
