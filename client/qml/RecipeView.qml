/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import org.jschwab.recipes 1.0

Page {
	id: recipeView

	property Recipe recipe

	ModalDialog {
		id: saveErrorDialog

		standardButtons: Dialog.Ok

		title: qsTr("Error")
		ColumnLayout {
			anchors.fill: parent

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("Can't save recipe.")
				wrapMode: Text.Wrap
			}
		}
	}

	SaveRecipeDialog {
		id: saveRecipeDialog
		onAccepted: if (!backend.saveRecipeToFile(recipeView.recipe, file)) saveErrorDialog.open()
	}

	ModalDialog {
		id: confirmDeleteDialog
		x: (mainWindow.width - width) / 2 - (Globals.mobile ? 0 : recipesListView.width)
		standardButtons: Dialog.Yes | Dialog.No

		title: qsTr("Delete recipe?")

		ColumnLayout {
			anchors.fill: parent

			Label {
				Layout.maximumWidth: mainWindow.width - 50
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("Realy delete recipe \"%1\"?").arg(recipe ? recipe.name : "")
				wrapMode: Text.Wrap
			}
		}

		onAccepted: function() {
			if (!backend.deleteRecipe(recipe)) concole.log("Can't delete recipe");
			if (Globals.mobile) stackView.pop();
		}
	}

	header: ToolBar {
		RowLayout {
			anchors.fill: parent

			BigLabel {
				id: name
				text: recipe ? recipe.name : ""
				horizontalAlignment: Qt.AlignHCenter
				verticalAlignment: Qt.AlignVCenter
				Layout.fillWidth: true
			}

			ToolButton {
				icon.name: "document-share"
				icon.source: "qrc:/img/fallback-icons/document-share.svg"
				onClicked: backend.shareRecipe(recipeView.recipe)
				ToolTip.text: qsTr("Share")
				ToolTip.delay: 500
				ToolTip.visible: hovered
				visible: recipe && Globals.mobile
			}

			ToolButton {
				icon.name: "document-save"
				icon.source: "qrc:/img/fallback-icons/document-save.svg"
				onClicked: saveRecipeDialog.open()
				ToolTip.text: qsTr("Save to file")
				ToolTip.delay: 500
				ToolTip.visible: hovered
				visible: recipe && !Globals.mobile
			}

			ToolButton {
				icon.name: "edit-delete"
				icon.source: "qrc:/img/fallback-icons/edit-delete.svg"
				onClicked: confirmDeleteDialog.open()
				ToolTip.text: qsTr("Delete recipe")
				ToolTip.delay: 500
				ToolTip.visible: hovered
				visible: recipe
			}

			ToolButton {
				icon.name: "document-edit"
				icon.source: "qrc:/img/fallback-icons/document-edit.svg"
				onClicked: stackView.editRecipe(recipe);
				ToolTip.text: qsTr("Edit recipe")
				ToolTip.delay: 500
				ToolTip.visible: hovered
				visible: recipe
			}
		}
	}

	ScrollView {
		visible: recipe
		anchors.fill: parent
		anchors.leftMargin: 5
		//ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
		//ScrollBar.vertical.policy: ScrollBar.AlwaysOn
		clip: true
		Flickable {
			boundsBehavior: Flickable.StopAtBounds
			ColumnLayout {
				Item {
					height: 5
				}

				RoundedImage {
					id: image
					Layout.maximumWidth: recipeView.width - 10
					fillMode: Image.PreserveAspectFit
					source: recipe ? recipe.image : ""
				}
		
				MediumLabel {
					text: recipe ? Math.floor(recipe.tmpPortions) == recipe.tmpPortions ? qsTr("Ingredients for %n portion(s):", "", recipe.tmpPortions) : qsTr("Ingredients for %1 portions:").arg(recipe.tmpPortions.toLocaleString(Qt.locale())) : ""
					visible: recipe ? recipe.ingredients.length : false
				}

				MediumLabel {
					text: qsTr("There aren't any ingredients for this recipe.")
					visible: recipe ? (recipe.ingredients.length == 0) : false
				}

				ListView {
					Layout.leftMargin: 10

					model: recipe ? recipe.ingredients : null

					delegate: Label {
						text: "\u2022 " + modelData.multipliedCount + " " + modelData.unit + (modelData.unit == "" ? "" : " ") + modelData.article
					}

					onModelChanged: Layout.preferredHeight = childrenRect.height
					Component.onCompleted: Layout.preferredHeight = childrenRect.height
					visible: recipe ? recipe.ingredients.length : false
				}

				GridLayout {
					columns: Globals.mobile ? 1 : 2

					MediumLabel {
						text: qsTr("Convert ingredients for portions:")
						wrapMode: Text.Wrap
					}

					DoubleSpinBox {
						id: newPortions
						realValue: recipe ? recipe.portions : 0
						onValueModified: recipe.tmpPortions = newPortions.realValue

					}
					visible: recipe ? recipe.ingredients.length : false
				}

				MediumLabel {
					text: qsTr("Instruction:")
					visible: recipe ? recipe.hasInstruction : false
				}

				Label {
					id: instruction
					Layout.leftMargin: 10
					Layout.maximumWidth: recipeView.width - 20
					Layout.preferredWidth: recipeView.width - 20
					text: recipe ? recipe.instruction : ""
					wrapMode: Text.Wrap
					visible: recipe ? recipe.hasInstruction : false
				}
			}
		}
	}

	onRecipeChanged: function() {
		if (recipe) {
			recipe.tmpPortions = recipe.portions;
			newPortions.realValue = recipe.portions;
			image.visible = recipe.hasImage;
		}
	}

	Connections {
		target: recipe
		onDataChanged: image.visible = recipe.hasImage;
	}
}
