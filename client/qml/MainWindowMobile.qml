/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

ApplicationWindow {
	id: mainWindow

	visible: true
	title: Qt.application.name

	StackView {
		id: stackView
		initialItem: RecipesListView {}
		anchors.fill: parent

		function selectRecipe(recipe) {
			push("qrc:/qml/RecipeView.qml", {"recipe": recipe});
		}

		function editRecipe(recipe) {
			push("qrc:/qml/RecipeEditView.qml", {"recipe": recipe});
		}

		function searchRecipes() {
			push("qrc:/qml/RecipesSearchView.qml");
		}

		function toSettings() {
			push("qrc:/qml/SettingsView.qml");
		}
	}

	property Dialog openDialog

	onClosing: function(close) {
		if (openDialog) {
			openDialog.reject();
			close.accepted = false;
			return;
		}
		if (stackView.depth > 1) {
			close.accepted = false;
			stackView.pop();
			backend.stopSharing();
			backend.stopSearching();
		}
	}

	Connections {
		target: backend
		onRecipeImported: stackView.selectRecipe(backend.getRecipe(id))
	}
}
