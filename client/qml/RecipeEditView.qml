/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.10
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import org.jschwab.recipes 1.0

Page {
	id: recipeEditView

	property Recipe recipe
	property bool committed: false

	OpenImageDialog {
		id: openImageDialog
		onAccepted: recipe.image = file
	}

	header: ToolBar {
		RowLayout {
			anchors.fill: parent

			BigLabel {
				text: qsTr("Edit recipe")
				horizontalAlignment: Qt.AlignHCenter
				verticalAlignment: Qt.AlignVCenter
				Layout.fillWidth: true
			}
		}
	}

	ScrollView {
		id: scrollView

		anchors.fill: parent
		anchors.leftMargin: 5
		//ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
		//ScrollBar.vertical.policy: ScrollBar.AlwaysOn
		clip: true
		Flickable {
			boundsBehavior: Flickable.StopAtBounds

			ColumnLayout {
				Item {
					height: 5
				}

				MediumLabel {
					text: qsTr("Name:")
				}

				TextField {
					Layout.leftMargin: 10
					Layout.preferredWidth: Globals.mobile ? recipeEditView.width - 20 : Layout.preferredWidth
					placeholderText: qsTr("Name")
					text: recipe ? recipe.name : ""
					onTextEdited: recipe.name = text
				}

				MediumLabel {
					text: qsTr("Image:")
				}

				RowLayout {
					Layout.leftMargin: 10
					spacing: 5

					RoundedImage {
						source: recipe ? recipe.image : ""
						Layout.maximumWidth: recipeEditView.width / 2
						fillMode: Image.PreserveAspectFit
					}

					ToolButton {
						icon.name: "document-open"
						icon.source: "qrc:/img/fallback-icons/document-open.svg"
						onClicked: {
							if (Globals.mobile) {
								backend.requestImage();
							} else {
								openImageDialog.visible = true;
							}
						}
						ToolTip.text: qsTr("Open image")
						ToolTip.delay: 500
						ToolTip.visible: hovered
					}

					ToolButton {
						icon.name: "edit-delete"
						icon.source: "qrc:/img/fallback-icons/edit-delete.svg"
						onClicked: recipe.image = ""
						ToolTip.text: qsTr("Delete image")
						ToolTip.delay: 500
						ToolTip.visible: hovered
						visible: recipe ? recipe.hasImage : false
					}
				}

				RowLayout {
					spacing: 5

					MediumLabel {
						text: qsTr("Ingredients for")
					}

					TextField {
						Layout.preferredWidth: 50
						validator: IntValidator{
							bottom: 1
							top: 999999
						}
						text: recipe ? recipe.portions : 1
						onTextEdited: recipe.portions = parseInt(text)
						inputMethodHints: Qt.ImhDigitsOnly
					}

					MediumLabel {
						text: qsTr("portion(s):")
					}
				}

				ListView {
					id: ingredientsList

					Layout.leftMargin: 10
					property int textWidth: 0

					model: recipe ? recipe.ingredients : null
					spacing: 5

					delegate: Row {
						spacing: 5
					
						TextField {
							placeholderText: qsTr("Count")
							text: modelData.count.toLocaleString(Qt.locale(), 'f', 3)
							width: 80 
							validator: DoubleValidator {
								bottom: 0
								decimals: 3
							}
							inputMethodHints: Qt.ImhFormattedNumbersOnly

							onEditingFinished: modelData.count = Number.fromLocaleString(Qt.locale(), text)
						}
					
						TextField {
							placeholderText: qsTr("Unit")
							width: 80 
							text: modelData.unit
							onEditingFinished: modelData.unit = text
						}

						TextField {
							placeholderText: qsTr("Ingredient")
							text: modelData.article
							onEditingFinished: modelData.article = text
						}

						ToolButton {
							icon.name: "edit-delete"
							icon.source: "qrc:/img/fallback-icons/edit-delete.svg"
							onClicked: recipe.deleteIngredient(modelData)
							ToolTip.text: qsTr("Delet ingredient")
							ToolTip.delay: 500
							ToolTip.visible: hovered
						}
					}

					onModelChanged: Layout.preferredHeight = childrenRect.height
					Component.onCompleted: if (recipe) recipe.refreshIngredients(); //Bloody hack to force rerendering
				}

				ToolButton {
					Layout.leftMargin: 10
					icon.name: "list-add"
					icon.source: "qrc:/img/fallback-icons/list-add.svg"
					onClicked: function() {
						recipe.addIngredient();
					}
					ToolTip.text: qsTr("Add ingredient")
					ToolTip.delay: 500
					ToolTip.visible: hovered
				}

				MediumLabel {
					text: qsTr("Instruction:")
				}

				TextArea {
					Layout.maximumWidth: recipeEditView.width - 20
					Layout.preferredWidth: recipeEditView.width - 20
					Layout.leftMargin: 10

					id: instruction
					height: 200
					placeholderText: qsTr("Instruction")
					text: recipe ? recipe.instruction : ""
					wrapMode: TextEdit.Wrap
					onEditingFinished: recipe.instruction = text
				}

			}
		}
	}

	footer: RowLayout {
		Item {
			Layout.fillWidth: true
		}

		Button {
			text: qsTr("Cancel")
			onClicked: stackView.pop();
			visible: !Globals.mobile
			Layout.bottomMargin: 5
		}

		Button {
			text: qsTr("Save")
			highlighted: Globals.mobile
			onClicked: function () {
				committed = true;
				backend.commitTransaction();
				stackView.pop();
			}
			Layout.bottomMargin: 5
			Layout.rightMargin: 5
		}
	}

	StackView.onActivating: {
		backend.startTransaction();
		if (!recipe) recipe = backend.getNewRecipe();
	}

	StackView.onDeactivating: {
		if (!committed) backend.rollbackTransaction();
	}

	Connections {
		target: backend
		onOpenImage: recipe.image = src
	}
}
