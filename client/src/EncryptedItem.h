/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ENCRYPTED_ITEM_H
#define ENCRYPTED_ITEM_H

#include "DataStructs.h"

#include <QByteArray>

class EncryptedItem {
	private:
		EncryptedItem(const QByteArray &id, const QByteArray &data);

	protected:
		QByteArray id, cipher, nonce, data;

	public:
		enum class Type {
			Recipe
		};

		EncryptedItem(const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce);
		QByteArray getId() const;
		QByteArray getCipher() const;
		QByteArray getNonce() const;
		static EncryptedItem fromRecipeData(const RecipeData&);
		Type getType() const;
};

class EncryptedRecipe : public EncryptedItem {
	public:
		explicit EncryptedRecipe(const EncryptedItem&);
		RecipeData getData() const;
};

#endif //ENCRYPTED_ITEM_H


