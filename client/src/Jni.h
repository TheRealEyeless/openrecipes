#ifndef JNI_H
#define JNI_H

#include <jni.h>
#include <forward_list>
#include <QString>
#include <QMutex>
#include <QObject>
#include <memory>

class JniNotifier : public QObject {
	Q_OBJECT

	signals:
		void recipeImported(const QByteArray &id);
};

class Jni {
	private:
		static std::shared_ptr<JniNotifier> notifier;

	public:
		static std::forward_list<QString> importBuffer;
		static QMutex importBufferMutex;
		static std::shared_ptr<JniNotifier> getNotifier();
};

extern "C" {
JNIEXPORT jboolean JNICALL Java_org_jschwab_openrecipes_Helpers_importRecipe(JNIEnv*, jclass, jstring);

JNIEXPORT jstring JNICALL Java_org_jschwab_openrecipes_Helpers_getRecipeXml(JNIEnv*, jclass, jstring);

JNIEXPORT jstring JNICALL Java_org_jschwab_openrecipes_Helpers_getRecipeName(JNIEnv*, jclass, jstring);
}

#endif //JNI_H
