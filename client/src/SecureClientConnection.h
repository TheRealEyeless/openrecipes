/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SECURE_CLIENT_CONNECTION_H
#define SECURE_CLIENT_CONNECTION_H

#include <SecureConnection.h>

class QHostAddress;

class SecureClientConnection : public SecureConnection {
	Q_OBJECT

	private:
		bool verifyRemotePublicKey(const QByteArray &key) final;
		static std::pair<QHostAddress, quint16> getHostNameAndPort();

	public:
		SecureClientConnection(bool authSelf);
};

#endif //SECURE_CLIENT_CONNECTION_H
