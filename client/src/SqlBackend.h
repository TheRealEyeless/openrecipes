/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SQL_BACKEND_H
#define SQL_BACKEND_H

#include <SqlBackendBase.h>

#include <QSqlDatabase>
#include <QObject>
#include <QMutex>
#include <memory>
#include <forward_list>

class Recipe;
class Ingredient;
class QStringList;
class Backend;
class EncryptedItem;
class EncryptedRecipe;

class SqlBackendTransmitter : public QObject {
	Q_OBJECT

	signals:
		void recipeChanged(const QByteArray &id);
		void ingredientChanged(int id);
		void recipeIdChanged(const QByteArray &oldId, const QByteArray &newId);
		void recipesListChanged();
		void syncSettingsChanged();
};

class SqlBackend : public SqlBackendBase {
	Q_OBJECT

	public:
		enum class Table {
			Recipes,
			Ingredients
		};

	private:
		static std::unique_ptr<SqlBackendTransmitter> transmitter;
		static QMutex mutex;

		static QSqlDatabase addDatabase(const QString &connectionName);
		static void createDatabase();
		template<class T, class S>
		static T getOneValue(Table table, const QString &valueName, const S &id);

		template<class T>
		static void setOneValueRecipes(const QString &valueName, const T &value, const QByteArray &id);
		template<class T>
		static void setOneValueIngredients(const QString &valueName, const T &value, int id);

	public:
		static void init();

		static QByteArray getSyncKey();
		static quint64 getLastSyncTimestamp();
		static void updateLastSyncTimestamp(quint64 timestamp);
		static void setSyncKey(const QByteArray &publicSyncKey, const QByteArray secretSyncKey);
		static bool getUseCustomSyncServer();
		static QString getCustomSyncServerAddr();
		static QByteArray getCustomSyncServerKey();
		static quint16 getCustomSyncServerPort();
		static void setCustomSyncServerAddr(const QString &addr);
		static void setCustomSyncServerPort(quint16 port);
		static void setCustomSyncServerKey(const QByteArray &key);
		static void setUseCustomSyncServer(bool use);

		/**
		 * Get all recipes.
		 */
		static QList<Recipe*> getRecipes(const QString &filter = "");
		static Recipe* getRecipe(const QByteArray &id);

		static QList<Ingredient*> getIngredientsForRecipe(const QByteArray &id);
		static QString getImageForRecipe(const QByteArray &id);
		static QString getNameForRecipe(const QByteArray &id);
		static QString getInstructionForRecipe(const QByteArray &id);
		static quint64 getPortionsForRecipe(const QByteArray &id);
		static void addIngredientToRecipe(const QByteArray &id, quint64 count, const QString &unit, const QString &article);
		static Recipe* addEmptyRecipe();

		static void setNameForRecipe(const QByteArray &id, const QString &name);
		static void setImageForRecipe(const QByteArray &id, const QString &name);
		static void setInstructionForRecipe(const QByteArray &id, const QString &instruction);
		static void setPortionsForRecipe(const QByteArray &id, quint64 portions);

		static quint64 getCountForIngredient(int id);
		static QString getUnitForIngredient(int id);
		static QString getArticleForIngredient(int id);
		static QByteArray getIdRecipeForIngredient(int id);

		static void setCountForIngredient(int id, quint64 count);
		static void setUnitForIngredient(int id, const QString &unit);
		static void setArticleForIngredient(int id, const QString &article);

		static void deleteRecipe(const QByteArray &id);
		static void deleteIngredient(int id);
		static void toDB(Recipe *recipe);
		static void toDB(const EncryptedItem&);
		static void toDB(const EncryptedRecipe&);

		static void deleteOthersIfNotChanged(const std::vector<QByteArray> &ids);
		static std::forward_list<std::pair<QByteArray, Table>> getChangedIds();
		static std::forward_list<QByteArray> getAllIds();
		static std::forward_list<EncryptedItem> getChangedEnc();
		static void changeId(const QByteArray &id, Table table);
		static void resetChanged(bool value);

		static void setupSync();
		static void setupSync(const QByteArray &symmeticKey, const QByteArray &publicKey, const QByteArray &secretKey);
		static void removeSyncSettings();

		static void connectSignals(Recipe *recipe);
		static void connectSignals(Ingredient *ingredient);
		static void connectSignals(Backend *backend);
		static void connectSignals(SqlTransaction *trans);
};

#endif //SQL_BACKEND_H
