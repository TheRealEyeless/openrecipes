/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "RecvSyncKeyAsync.h"
#include "SqlBackend.h"
#include "SecureClientConnection.h"
#include "EncryptedItem.h"
#include "Globals.h"

#include <InternalError.h>
#include <FixedSizeNetworkPackage.h>

#include <cassert>
#include <QTcpSocket>
#include <QtEndian>
#include <QEventLoop>

RecvSyncKeyAsync::RecvSyncKeyAsync(QObject *parent, const QByteArray &key)
:
	QThread(parent),
	stop(false),
	connection(nullptr),
	key(key)
{
	if (key.size() != crypto_secretbox_KEYBYTES) throw IERROR("Key has invalid size");
}

RecvSyncKeyAsync::~RecvSyncKeyAsync() {
	mutex.lock();
	stop = true;
	mutex.unlock();
	if (connection) connection->deleteLater();
	connection = nullptr;
}

void RecvSyncKeyAsync::checkStop() {
	bool tmp;
	mutex.lock();
	tmp = stop;
	mutex.unlock();
	if (tmp) throw IERROR("STOP");
}

void RecvSyncKeyAsync::run() {
	bool succ = true;
	try {
		createConnection();
		checkStop();
		sendRequest();
		checkStop();
		recvSyncKey();
	} catch (const InternalError &e) {
		succ = false;
	}
	emit done(succ);
	if (connection) connection->deleteLater();
	connection = nullptr;
}

void RecvSyncKeyAsync::createConnection() {
	assert(connection == nullptr);
	connection = new SecureClientConnection(false);
	connect(connection, &SecureClientConnection::disconnected, [this]() {
			qDebug() << "Connection was unexpectedly closed";
			mutex.lock();
			stop = true;
			mutex.unlock();
			}
	);
	QEventLoop loop;
	connect(connection, &SecureClientConnection::cryptoEstablished, &loop, &QEventLoop::quit);
	connect(connection, &SecureClientConnection::disconnected, &loop, &QEventLoop::quit);
	loop.exec();
	//TODO check stop
}

void RecvSyncKeyAsync::sendRequest() {
	connection->write("GETSK");

	assert(16 >= crypto_generichash_BYTES_MIN && 16 <= crypto_generichash_BYTES_MAX);
	QByteArray id;
	id.resize(16);
	crypto_generichash(
			reinterpret_cast<unsigned char*>(id.data()),
			16,
			reinterpret_cast<const unsigned char*>(key.constData()),
			key.size(),
			nullptr,
			0
	);

	QByteArray size;
	size.resize(8);
	*reinterpret_cast<quint64*>(size.data()) = qToBigEndian<quint64>(16);
	connection->write(size);
	connection->write(id);
}

void RecvSyncKeyAsync::recvSyncKey() {
	FixedSizeNetworkPackage sizeNP(16);
	sizeNP.readBlocking(connection);

	const quint64 cipherSize = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(sizeNP.getData().constData()));
	const quint64 nonceSize = qFromBigEndian<quint64>(*reinterpret_cast<const quint64*>(sizeNP.getData().constData() + 8));

	if (cipherSize != crypto_sign_PUBLICKEYBYTES + crypto_sign_SECRETKEYBYTES + crypto_secretbox_MACBYTES) throw IERROR("Cipher has invalid length");

	FixedSizeNetworkPackage data(cipherSize + nonceSize);
	data.readBlocking(connection);

	QByteArray plainData;
	plainData.resize(cipherSize - crypto_secretbox_MACBYTES);
	if (crypto_secretbox_open_easy(
				reinterpret_cast<unsigned char*>(plainData.data()),
				reinterpret_cast<const unsigned char*>(data.getData().constData()),
				cipherSize,
				reinterpret_cast<const unsigned char*>(data.getData().constData() + cipherSize),
				reinterpret_cast<const unsigned char*>(key.constData())
	) != 0) throw IERROR("Can't decrypt key");

	const QByteArray publicKey(plainData.constData(), crypto_sign_PUBLICKEYBYTES);
	const QByteArray secretKey(plainData.constData() + crypto_sign_PUBLICKEYBYTES, crypto_sign_SECRETKEYBYTES);

	SqlBackend::setupSync(key, publicKey, secretKey);
}

void RecvSyncKeyAsync::cancel() {
	mutex.lock();
	stop = true;
	mutex.unlock();
}
