/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Recipe.h"
#include "SqlBackend.h"
#include "Globals.h"

#include <InternalError.h>

#include <QBuffer>
#include <QDir>
#include <QPainter>
#include <QPixmap>
#include <QSvgRenderer>
#include <QTextStream>
#include <QVariant>
#include <QXmlAttributes>
#include <QXmlDefaultHandler>
#include <QXmlSimpleReader>
#include <QtEndian>
#include <array>
#include <cassert>
#include <cstring>

Recipe::Recipe(const QByteArray &id, const QList<Ingredient*> ingredients)
:
	id(id),
	ingredients(ingredients)
{
	for (auto &i : ingredients) i->setParent(this);
}

Recipe::Recipe(const QString &xml, QObject *parent)
:
	QObject(parent)
{
	class RecipeXmlHandler : public QXmlDefaultHandler {
		private:
			Recipe *recipe;

		public:
			explicit RecipeXmlHandler(Recipe *recipe) : recipe(recipe) {}
			bool fatalError(const QXmlParseException &e) override {
				qDebug() << e.message();
				throw IERROR("Fatal parser error");
			}
			bool startElement(const QString&, const QString&, const QString &qName, const QXmlAttributes &atts) {
				if (qName == "recipe") {
					recipe->setName(atts.value("name"));
					recipe->setImage(atts.value("image"));
					recipe->setPortions(atts.value("portions").toULongLong());
					recipe->setTmpPortions(recipe->getPortions());
					recipe->setInstruction(atts.value("instruction"));
					return true;
				}
				if (qName == "ingredient") {
					SqlBackend::addIngredientToRecipe(recipe->getId(), atts.value("count").toULongLong(), atts.value("unit"), atts.value("article"));
					return true;
				}
				qCritical() << "Unknown xml tag";
				return false;
			}
	};

	SqlTransaction trans;
	SqlBackend::connectSignals(&trans);
	Recipe *tmp = SqlBackend::addEmptyRecipe();
	id = tmp->getId();
	delete tmp;
	QXmlInputSource source;
	source.setData(xml);
	QXmlSimpleReader xmlReader;
	RecipeXmlHandler *handler = new RecipeXmlHandler(this);
	xmlReader.setContentHandler(handler);
	xmlReader.setErrorHandler(handler);
	if (!xmlReader.parse(source)) throw IERROR("Can't parser xml");
	trans.commit();
	SqlBackend::connectSignals(this);
	ingredients = SqlBackend::getIngredientsForRecipe(id);
}

QByteArray Recipe::getId() const {
	return id;
}

bool Recipe::hasImage() const {
	try {
		return !SqlBackend::getImageForRecipe(id).isEmpty();
	} catch (const SqlNoResult &e) {
		return false;
	}
}

bool Recipe::hasInstruction() const {
	try {
		return !getInstruction().isEmpty();
	} catch (const SqlNoResult &e) {
		return false;
	}
}

QString Recipe::getName() const {
	try {
		return SqlBackend::getNameForRecipe(id);
	} catch (const SqlNoResult &e) {
		return QString();
	}
}

QString Recipe::getSmallImage() const {
	return getImage(Globals::getRecipesListImageHeight());
}

QString Recipe::getImage(unsigned int height) const {
	QString image;
	try {
		image = SqlBackend::getImageForRecipe(id);
	} catch (const SqlNoResult &e) {
		return QString();
	}
	if (image.isEmpty()) {
		QSvgRenderer renderer(QString(":/img/icon_plain.svg"));
		QImage i(height, height, QImage::Format_ARGB32);
		const std::array<QColor, 6> colors = {
			QColor(0xdc, 0xe7, 0x75), //Lime_300
			QColor(0x81, 0xc7, 0x84), //Green_300
			QColor(0x4d, 0xd0, 0xe1), //Cyan_300
			QColor(0x95, 0x75, 0xcd), //Deep_Purple_300
			QColor(0xf0, 0x62, 0x92), //Pink_300
			QColor(0x64, 0xb5, 0xf6)  //Blue_300
		};
		i.fill(colors[id[0] % colors.size()]);
		QPainter painter(&i);
		renderer.render(&painter);

		QByteArray a;
		QBuffer b(&a);
		b.open(QIODevice::WriteOnly);
		i.save(&b, "PNG");
		QString img("data:image/png;base64,");
		img.append(QString::fromLatin1(a.toBase64().data()));
		return img;
	} else {
		return image;
	}
}

unsigned int Recipe::getPortions() const {
	try {
		return SqlBackend::getPortionsForRecipe(id);
	} catch (const SqlNoResult &e) {
		return 0;
	}
}

double Recipe::getTmpPortions() const {
	return tmpPortions;
}

int Recipe::getIngredientsCount() const {
	return ingredients.size();
}

Ingredient* Recipe::getIngredientAt(int pos) {
	return ingredients.at(pos);
}

QQmlListProperty<Ingredient> Recipe::getIngredientsQml() {
	return QQmlListProperty<Ingredient>(
			this,
			this,
			[](QQmlListProperty<Ingredient> *list) {
				return reinterpret_cast<Recipe*>(list->data)->getIngredientsCount();
			},
			[](QQmlListProperty<Ingredient> *list, int pos) {
				return reinterpret_cast<Recipe*>(list->data)->getIngredientAt(pos);
			}
	);
}

void Recipe::addIngredient() const {
	SqlBackend::addIngredientToRecipe(id, 0, "", "");
}

void Recipe::deleteIngredient(Ingredient *ingredient) const {
	SqlBackend::deleteIngredient(ingredient->getId());
}

QString Recipe::getInstruction() const {
	try {
		return SqlBackend::getInstructionForRecipe(id);
	} catch (const SqlNoResult &e) {
		return QString();
	}
}

void Recipe::onRecipeChanged(const QByteArray &id) {
	if (id.isEmpty() || this->id == id) {
		for (auto &i : ingredients) i->deleteLater();
		ingredients.clear();
		try {
			ingredients = SqlBackend::getIngredientsForRecipe(this->id);
		} catch (const SqlNoResult &e) {}
		emit dataChanged();
	}
}

void Recipe::onRecipeIdChanged(const QByteArray &oldId, const QByteArray &newId) {
	if (id == oldId) {
		id = newId;
		emit idChanged();
	}
}

void Recipe::setName(const QString &name) {
	SqlBackend::setNameForRecipe(id, name);
}

void Recipe::setImage(const QString &imageSrc) {
	if (imageSrc.isEmpty()) {
		SqlBackend::setImageForRecipe(id, "");
	} else {
		QString image;
		const QString begin("data:image/png;base64,");
		if (imageSrc.left(begin.size()) == begin) {
			image = imageSrc;
		} else {
			qDebug() << "Opening image" << imageSrc;
			const QString resolvedPath = QUrl::fromUserInput(imageSrc, QDir::currentPath(), QUrl::AssumeLocalFile).toLocalFile();
			QImage i(resolvedPath);
			//If you are changing size here, change it in java helper class too.
			i = i.scaledToHeight(120, Qt::SmoothTransformation);
			QByteArray imageData;
			QBuffer buffer(&imageData);
			buffer.open(QIODevice::WriteOnly);
			i.save(&buffer, "PNG");
			image = begin;
			image.append(QString::fromLatin1(imageData.toBase64().data()));
		}
		SqlBackend::setImageForRecipe(id, image);
	}
}

void Recipe::setPortions(unsigned int portions) {
	SqlBackend::setPortionsForRecipe(id, portions);
	setTmpPortions(portions);
}

void Recipe::setTmpPortions(double tmpPortions) {
	this->tmpPortions = tmpPortions;
	for (auto &i : ingredients) i->setMultiplier(tmpPortions / static_cast<double>(getPortions()));
	emit tmpPortionsChanged();
}

void Recipe::setInstruction(const QString &instruction) {
	SqlBackend::setInstructionForRecipe(id, instruction);
}

void Recipe::delImage() {
	SqlBackend::setImageForRecipe(id, "");
}

void Recipe::refreshIngredients() {
	emit dataChanged();
}

QString Recipe::toXML() const {
	QString xml;
	QTextStream x(&xml);
	x << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
	x << "<recipe ";
	x << "name=\"" << getName() << "\" ";
	x << "image=\"" << (hasImage() ? getImage() : "") << "\" ";
	x << "portions=\"" << getPortions() << "\" ";
	x << "instruction=\"" << getInstruction() << "\">";
	for (const auto &i : ingredients) {
		x << "<ingredient ";
		x << "count=\"" << static_cast<quint64>(i->getCount() * 1000) << "\" ";
		x << "unit=\"" << i->getUnit() << "\" ";
		x << "article=\"" << i->getArticle() << "\" /> ";
	}
	x << "</recipe>";
	return xml;
}
