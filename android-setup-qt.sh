#!/bin/bash

QT_ANDROID_INSTALLER_SHA256=a214084e2295c9a9f8727e8a0131c37255bf724bfc69e80f7012ba3abeb1f763


## $1 filename, $2 sha256 hash
function check_sha256()
{
	echo ${2} "${1}" | sha256sum -c &> /dev/null
}

## $1 filename, $2 sha256 hash, $3 url
function verified_download()
{
	FILENAME="$1"
	SHA256="$2"
	URL="$3"

	check_sha256 "${FILENAME}" "${SHA256}" ||
	{
		rm -rf "${FILENAME}"

		wget -O "${FILENAME}" "$URL" ||
		{
			echo "Failed downloading ${FILENAME} from $URL"
			exit 1
		}

		check_sha256 "${FILENAME}" "${SHA256}" ||
		{
			echo "SHA256 mismatch for ${FILENAME} from ${URL} expected sha256 ${SHA256} got $(sha256sum ${FILENAME} | awk '{print $1}')"
			exit 1
		}
	}
}


## More information available at https://gitlab.com/relan/provisioners/merge_requests/1 and http://stackoverflow.com/a/34032216
install_qt_android()
{
	QT_VERSION_CODE=$(echo $QT_VERSION | tr -d .)
	QT_INSTALLER="qt-unified-linux-x64-3.0.2-online.run"

	verified_download $QT_INSTALLER $QT_ANDROID_INSTALLER_SHA256 \
		http://master.qt.io/archive/online_installers/3.0/${QT_INSTALLER}

	chmod a+x ${QT_INSTALLER}

	QT_INSTALLER_SCRIPT="qt_installer_script.js"
	cat << EOF > "${QT_INSTALLER_SCRIPT}"
function Controller() {
	installer.autoRejectMessageBoxes();
	installer.installationFinished.connect(function() {
		gui.clickButton(buttons.NextButton);
	});

	var welcomePage = gui.pageWidgetByObjectName("WelcomePage");
	welcomePage.completeChanged.connect(function() {
		if (gui.currentPageWidget().objectName == welcomePage.objectName)
			gui.clickButton(buttons.NextButton);
	});
}

Controller.prototype.WelcomePageCallback = function() {
	gui.clickButton(buttons.NextButton);
}

Controller.prototype.CredentialsPageCallback = function() {
	gui.clickButton(buttons.NextButton);
}

Controller.prototype.IntroductionPageCallback = function() {
	gui.clickButton(buttons.NextButton);
}

Controller.prototype.TargetDirectoryPageCallback = function() {
	gui.currentPageWidget().TargetDirectoryLineEdit.setText("$QT_INSTALL_PATH");
	gui.clickButton(buttons.NextButton);
}

Controller.prototype.ComponentSelectionPageCallback = function() {
	var widget = gui.currentPageWidget();

	// You can get these component names by running the installer with the
	// --verbose flag. It will then print out a resource tree.

	widget.deselectAll();
	widget.selectComponent("qt.qt5.$QT_VERSION_CODE.android_armv7");

	gui.clickButton(buttons.NextButton);
}

Controller.prototype.LicenseAgreementPageCallback = function() {
	gui.currentPageWidget().AcceptLicenseRadioButton.setChecked(true);
	gui.clickButton(buttons.NextButton);
}

Controller.prototype.StartMenuDirectoryPageCallback = function() {
	gui.clickButton(buttons.NextButton);
}

Controller.prototype.ReadyForInstallationPageCallback = function() {
	gui.clickButton(buttons.NextButton);
}

Controller.prototype.FinishedPageCallback = function() {
	var checkBoxForm = gui.currentPageWidget().LaunchQtCreatorCheckBoxForm;
	if (checkBoxForm && checkBoxForm.launchQtCreatorCheckBox)
		checkBoxForm.launchQtCreatorCheckBox.checked = false;
	gui.clickButton(buttons.FinishButton);
}
EOF

QT_QPA_PLATFORM=minimal ./${QT_INSTALLER} --script ${QT_INSTALLER_SCRIPT}
}

install_qt_android
