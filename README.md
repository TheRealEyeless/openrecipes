# OpenRecipes

OpenRecipes is a privacy friendly personal cook book.

Most notable feature is the possibility so synchronize your recipes end-to-end encrypted between multiple devices.

Other features are:

* Edit your recipes with images
* Calculate ingredients for different number of portions
* Save and import recipes to/from file
* On Android: share recipes via other apps

The client should run on all major platforms (Android, Linux and Windows are tested).
Out of the box the client works completely offline<sup>1</sup>, the server is only needed for synchronization.
There is a default server preconfigured, but can be replaced by a costume server in the settings.
For details about the synchronization protocol please see `doc/synchronization_protocol.md`.

The server only runs on Linux.
It needs a mysql compatible database and is configured with an ini style file like this:

	[general]
	;number of jobs to run in the background (>=1) (optional, default: 2)
	jobs=2

	[server]
	;port to listen for connections
	port=44556

	[mysql]
	;mysql user name
	user=your_username
	;mysql password
	password=your_password
	;mysql database
	database=yout_database
	;host of the mysql server (optional, default: localhost)
	host=hostname
	;port the mysql server listens to (optional, default: 3306)
	port=3306

You also need to apply the latest db schema from `server/dbSchema` to the database you intend to use.

While the server is running, he will print out some status information when receiving SIGUSR1 (for example with `killall -USR1 openrecipesserver`).

## Binary releases

For prebuild releases please refer to <https://openrecipes.jschwab.org>.

## Build from source

To build from source you will need:

* Qt >= 5.11
* libsodium >= 1.0.12
* libqrencode

You can build everything with

	qmake
	make

If you want to build only specific parts (for example if your are building on a sever that doesn't has the qt GUI parts installed), you can run `qmake` and `make` in the specific subdirectory.
You always need to build lib though.
E.g. to only build the server do something like

	cd lib
	qmake
	make
	cd ../server
	qmake
	make

---

<sup>1</sup> On Windows, the client will check for updates on startup by fetching `https://api.openrecipes.jschwab.org/currentClientVersion.txt`.
Since there isn't any package manager on Windows, thats the only way to promote any updates.
