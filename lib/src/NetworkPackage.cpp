/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "NetworkPackage.h"
#include "SecureConnection.h"
#include "InternalError.h"

#include <cassert>
#include <QEventLoop>

NetworkPackage::NetworkPackage()
:
	connection(nullptr)
{}

bool NetworkPackage::completed() const {
	return !bytesNeeded();
}

void NetworkPackage::read(Connection *connection) {
	this->connection = connection;
	connect(connection, &Connection::readyRead, this, &NetworkPackage::onReadyRead);
	onReadyRead(); //Call it once in case there is data left from a previous package.
}

void NetworkPackage::readBlocking(Connection *connection) {
	read(connection);
	if (!completed()) {
		QEventLoop loop;
		connect(this, &NetworkPackage::complete, &loop, &QEventLoop::quit);
		connect(connection, &Connection::disconnected, &loop, &QEventLoop::quit);
		loop.exec();
		//TODO Make this cancelable
	}
	if (!completed()) throw IERROR("Error while reading NetworkPackage");
}

void NetworkPackage::onReadyRead() {
	while(connection->bytesAvailable() && !completed()) {
		qDebug() << "Reading up to " << bytesNeeded() << " bytes";
		data += connection->read(bytesNeeded());
	}
	if (completed()) {
		disconnect(connection);
		emit complete();
	}
}

const QByteArray& NetworkPackage::getData() const {
	assert(completed());
	return data;
}
