/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NETWORK_PACKAGE_H
#define NETWORK_PACKAGE_H

#include <QObject>

class Connection;

/*!
 * \brief Class to read a network package.
 */
class NetworkPackage : public QObject{
	Q_OBJECT

	private:
		/*!
		 * \brief Connection to read from.
		 */
		Connection *connection;

	private slots:
		/*!
		 * \brief Handle new data on the connection.
		 */
		void onReadyRead();
	
	protected:
		/*!
		 * \brief Allready read data.
		 */
		QByteArray data;

		/*!
		 * \brief Number of bytes that still need to be read.
		 */
		virtual quint64 bytesNeeded() const = 0;

	public:
		/*!
		 * \brief Constructor.
		 */
		NetworkPackage();

		/*!
		 * \brief Read data.
		 * \param connection connection to read from
		 * \sa readBlocking()
		 *
		 * When reading is finished, complete() is emitted.
		 */
		void read(Connection *connection);

		/*!
		 * \brief Read data.
		 * \param connection connection to read from
		 * \exception InternalError when an error occures while reading
		 * \sa read()
		 *
		 * This function blocks untill all data is read.
		 */
		void readBlocking(Connection *connection);

		/*!
		 * \brief Get the data that has been read.
		 */
		const QByteArray& getData() const;

		/*!
		 * \brief Check if the package is read completly.
		 * \return true if all data has been read, false otherwise
		 */
		bool completed() const;

	signals:
		/*!
		 * \brief Emitted when all data has been read.
		 */
		void complete();
};

#endif //NETWORK_PACKAGE_H
