/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SQL_BACKEND
#define SQL_BACKEND

#include <SqlBackendBase.h>

#include <QSqlDatabase>
#include <vector>
#include <memory>
#include <QTimer>

struct DbItem;

class SqlBackend : public SqlBackendBase {
	public:
		struct Config {
			QString user;
			QString host;
			QString database;
			QString password;
			int port;
		};

	private:
		static std::unique_ptr<QTimer> cleanupTimer;
		static void cleanup();

		static QSqlDatabase addDatabase(const QString &connectionName);
		static Config conf;

	public:
		static void init(const Config &conf);
		static quint64 getUserId(const QByteArray &key);
		static std::vector<QByteArray> getIds(quint64 userId);
		static void addOrUpdate(const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce, quint64 userId);
		static void deleteEntry(const QByteArray &id, quint64 userId);
		static std::vector<DbItem> getChanged(quint64 timestamp, quint64 userId);
		static std::pair<QByteArray, QByteArray> getAndDelSyncKey(const QByteArray &id);
		static void addSyncKey(const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce);
		static unsigned long getNumberOfTodaysUsers();
};

#endif //SQL_BACKEND
