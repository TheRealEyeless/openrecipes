#!/bin/bash

set -o xtrace

relScriptPath=`dirname "${BASH_SOURCE[0]}"`
scriptPath=`realpath $relScriptPath`
libsPath="${scriptPath}/androidlibs"

LIB_SODIUM_DIR="libsodium-1.0.16"
LIB_SODIUM_TAR="libsodium-1.0.16.tar.gz"

TARGET_ARM="arm-linux-androideabi"
TARGET_x86="i686-linux-android"

tar -xf $LIB_SODIUM_TAR
cd $LIB_SODIUM_DIR

export CFLAGS="-D_FORTIFY_SOURCE=2"

oldPATH=$PATH

export PATH=$oldPATH:${scriptPath}/android-ndk-toolchain/arm/bin
./configure --host $TARGET_ARM --prefix ${libsPath}/arm
make -j5 V=1
make install

make clean

export PATH=$oldPATH:${scriptPath}/android-ndk-toolchain/x86/bin
./configure --host $TARGET_x86 --prefix ${libsPath}/x86
make -j5 V=1
make install
